<?php
header('Content-type: application/json; charset=UTF-8');

#sleep(1);

$f3 = require('lib/base.php');
require_once 'db.php';

////////////////////////////////////////////////////////////////////////////////
///
/// U S E R S
///
////////////////////////////////////////////////////////////////////////////////

$f3->route('POST /users/authorize',
  function($f3, $params) {
    $data = json_decode(file_get_contents('php://input'), true);
    if ($data['username'] != '' and $data['password'] != '') {
      if ($data['username'] == 'admin' and $data['password'] == '1234') {
        $r = [
          'result' => true,
          'data' => [
            'id' => 1,
            'username' => 'admin',
            'role' => 'admin',
          ],
          'msg' => 'ok, you are logged in'
        ];
        $_SESSION['user'] = 1;
      } else {
        $r = [
          'result' => false,
          'data' => [
            'username' => false,
            'role' => false,
          ],
          'msg' => 'Invalid username or password, logged out'
        ];
        unset($_SESSION['user']);
      }
    } else {
      $r = [
        'result' => false,
        'data' => [
          'username' => false,
          'role' => false,
        ],
        'msg' => 'Username or password empty, logged out'
      ];
      unset($_SESSION['user']);
    }
    echo json_encode($r);
  }
);

////////////////////////////////////////////////////////////////////////////////
///
/// T O D O S
///
////////////////////////////////////////////////////////////////////////////////

// Elenco dei todo
$f3->route('GET /todoes',
  function($f3, $params) {
    echo json_encode([
      'result' => true,
      'data' => get_todoes(),
      'msg' => 'ok'
    ]);
  }
);

// Elenco dei todo
$f3->route('GET /todoes/@id',
  function($f3, $params) {
    $todo = get_todo($params['id']);
    if ($todo) {
      $r = [
        'result' => true,
        'data' => [$todo],
        'msg' => 'ok'
      ];
    } else {
      $r = [
        'result' => false,
        'data' => [],
        'msg' => 'ko'
      ];
      http_response_code(404);
    }
    echo json_encode($r);
  }
);

// Inserire un nuovo todo
$f3->route('POST /todoes',
  function($f3, $params) {
    $data = json_decode(file_get_contents('php://input'), true);
    if ($data['todo'] != '' and $data['id_list'] > 0) {
      $todo = add_todo($data['todo'], $data['id_list']);
      $r = [
        'result' => true,
        'data' => [$todo],
        'msg' => 'ok'
      ];
    } else {
      $r = [
        'result' => false,
        'data' => [],
        'msg' => 'Ghe la fo mia'
      ];
      http_response_code(400);
    }
    echo json_encode($r);
  }
);

// Modificare un todo
$f3->route('PUT /todoes/@id',
  function($f3, $params) {
    $data = json_decode(file_get_contents('php://input'), true);
    if ($params['id'] > 0 and $data['todo'] != '' and $data['id_list'] > 0) {
      $todo = save_todo($params['id'], $data['todo'], $data['done'] ?? 0, $data['id_list']);
      $r = [
        'result' => true,
        'data' => [$todo],
        'msg' => 'ok'
      ];
      http_response_code(201);
    } else {
      $r = [
        'result' => false,
        'data' => [],
        'msg' => 'Ghe la fo mia'
      ];
      http_response_code(400);
    }
    echo json_encode($r);
  }
);

// Eliminare un todo
$f3->route('DELETE /todoes/@id',
  function($f3, $params) {
    $todo = get_todo($params['id']);
    del_todo($params['id']);
    if ($todo) {
      $r = [
        'result' => true,
        'data' => [$todo],
        'msg' => 'ok'
      ];
    } else {
      $r = [
        'result' => false,
        'data' => [],
        'msg' => 'El ghè mia'
      ];
      http_response_code(404);
    }
    echo json_encode($r);
  }
);

////////////////////////////////////////////////////////////////////////////////
///
/// L I S T S
///
////////////////////////////////////////////////////////////////////////////////

// Elenco delle liste
$f3->route('GET /lists',
  function($f3, $params) {
    echo json_encode([
      'result' => true,
      'data' => get_lists(),
      'msg' => 'ok'
    ]);
  }
);



$f3->route('GET /*',
  function() {
    echo "Hello, I'm the catch em all!";
  }
);

$f3->run();

